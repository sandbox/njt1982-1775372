<?php
/**
 * @file ctools_cck_field.ctools.autocomplete.inc
 *
 * This include is simple used for the Autocomplete callback for field names.
 */


/**
 * Helper function for autocompletion of node titles.
 */
function ctools_cck_field_autocomplete_field_name($string) {
  if ($string != '') {
    $string = drupal_strtolower($string);

    $result = db_query_range(db_rewrite_sql("SELECT f.field_name FROM {content_node_field} f WHERE f.field_name LIKE '%%%s%%'"), $string, 0, 10);

    $matches = array();
    while ($field = db_fetch_object($result)) {
      $matches[$field->field_name] = $field->field_name;
    }
    drupal_json($matches);
  }
}

