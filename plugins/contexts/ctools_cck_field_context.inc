<?php

/**
 * @file
 */

$plugin = array(
  'title' => t('CCK Field'),
  'description' => t('A context that contains CCK Fields from content.module.'),
  'context' => 'ctools_cck_field_ctools_cck_field_context_create',  // func to create context
  'keyword' => 'field_name',
  'context name' => 'cck_field',
  'convert list' => 'ctools_cck_field_ctools_cck_field_context_convert_list',
  'convert' => 'ctools_cck_field_ctools_cck_field_context_convert',
  'js' => array('misc/autocomplete.js'),
);


/**
 * Create a context from manual configuration.
 */
function ctools_cck_field_ctools_cck_field_context_create($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('ctools_cck_field_context');
  $context->plugin = 'ctools_cck_field_context';

  if ($empty || empty($data)) {
    return $context;
  }

  if ($conf) {
    $context->conf = $conf;
  }

  if (!empty($data)) {
    $context->data = $data;
    $context->title = t('Field: %field', array('%field' => $data['widget']['label']));
    $context->argument = $data['field_name'];
    $context->description = t('Data matching %field', array('%field' => $data['field_name']));
  }

  return $context;
}


/**
 * Convert List callback - provides the "types" of output for this context
 */
function ctools_cck_field_ctools_cck_field_context_convert_list() {
  // TODO - what if the field doesn't support value (eg email, date, etc)
  $list = array(
    'value' => t('Raw Value'),
    'safe' => t('HTML Safe Value'),
    'field_name' => t('Field Name'),
    'field_label' => t('Field Label'),
  );

  // TODO tokens?

  return $list;
}


/**
 * Callback for the Convert function.
 */
function ctools_cck_field_ctools_cck_field_context_convert($context, $type) {
  // Extract the field
  $field = isset($context->data['field']) ? $context->data['field'] : array();

  // If the type is a "simple" one, just return it now to save time
  switch ($type) {
    case 'field_name'  : return isset($field['field_name']) ? check_plain($field['field_name']) : '';
    case 'field_label' : return isset($field['widget']['label']) ? check_plain($field['widget']['label']) : '';
  }

  // Extract the rest of the data
  $node = isset($context->data['node']) ? $context->data['node'] : NULL;
  $conf = isset($context->conf) ? $context->conf : array();

  $value = NULL;
  if (isset($node) && isset($field['field_name']) && isset($node->{$field['field_name']})) {
    // Get the delta; assume 0 (zero) if not set.
    $delta = isset($conf['delta']) ? $conf['delta'] : 0;

    // Get the items
    $items = $node->{$field['field_name']};

    // If the type is "safe", sanitize
    if ($type == 'safe') {
      // Inspired by content_view_field() - one-off equivalent  to _content_field_invoke('sanitize').
      $function = $field['module'] .'_field';
      if (function_exists($function)) {
        $function('sanitize', $node, $field, $items, FALSE, FALSE);
      }
    }
    
    // Get the value
    $value = isset($items[$delta][$type]) ? $items[$delta][$type] : NULL;
  }

  return $value;
}
