<?php

/**
 * @file
 * Plugin to provide an relationship handler for CCK Fields.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('CCK Field from node'),
  'keyword' => 'field_name',
  'description' => t('Adds a CCK Field from a node context.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'ctools_cck_field_ctools_cck_field_relationship_context',
  'settings form' => 'ctools_cck_field_ctools_cck_field_relationship_settings_form',
  'settings form validate' => 'ctools_cck_field_ctools_cck_field_relationship_settings_form_validate',
  'defaults' => array(
    'field_name' => '',
    'delta' => 0,
    'column' => NULL,
  ),
  'js' => array('misc/autocomplete.js'),
);


/**
 * Return a new context based on an existing context.
 */
function ctools_cck_field_ctools_cck_field_relationship_context($context, $conf) {
  $data = array('node' => NULL, 'field' => NULL);

  if (isset($context->data->type)) {
    $data['node'] = $context->data;

    if ($field = content_fields($conf['field_name'], $context->data->type)) {
      // If we're here then the field IS instanced on this type
      $data['field'] = $field;
    }
    else {
      // If we're here then this content type doesn't instance this field...
      return ctools_context_create_empty('ctools_cck_field_context', NULL);
    }
  }
  else {
    // There is no content type - lets just try to get the field...
    if ($field = content_fields($conf['field_name'])) {
      // If we're here then we have a field, but no type...
      $data['field'] = $field;
    }
    else {
      // If we're here then we have not type at all - this is bad!
      // TODO: Should probably throw an error or watchdog here?!
      return ctools_context_create_empty('ctools_cck_field_context', NULL);
    }
  }

  // If we're here then good things have happend...
  return ctools_context_create('ctools_cck_field_context', $data, $conf);
}


/**
 * Settings form for the relationship.
 */
function ctools_cck_field_ctools_cck_field_relationship_settings_form($conf) {
  $form['field_name'] = array(
    '#title' => t('Field Name'),
    '#description' => t('Enter the machine name of the field to relate to. These always start <code>field_</code>. The field will autocomplete suggestions which contain the value you enter.'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 32,
    '#autocomplete_path' => 'ctools/autocomplete/ctools-cck-field-fied-names',
    '#default_value' => $conf['field_name'],
    '#required' => TRUE,
  );

  $form['delta'] = array(
    '#title' => t('Delta'),
    '#description' => t('The <em>delta</em> represents the item of the field. This is a zero-indexed list (0 (zero) is first followed by 1, 2, 3, etc). If the field is not set to multiple, you should leave this as zero.'),
    '#type' => 'textfield',
    '#maxlength' => 2,
    '#size' => 2,
    '#default_value' => $conf['delta'],
    '#required' => TRUE,
  );


  // If the column is not set and the field is, get the first column as a default.
  if (empty($conf['column']) && !empty($conf['field_name']) && $field_info = content_fields($conf['field_name'])) {
    $conf['column'] = array_shift(array_keys($field_info['columns']));
  }
  $form['column'] = array(
    '#title' => t('Column'),
    '#description' => t('The <em>column</em> represents the internal value of the field. This is <strong>usually</strong> <code>value</code>, however some fields use other columns. If you are unsure, you should leave this as the default suggestion.'),
    '#type' => 'textfield',
    '#maxlength' => 64,
    '#size' => 16,
    '#default_value' => $conf['column'],
    '#required' => TRUE,
  );

  return $form;
}


/**
 * Validate handler for the settings form. Ensure the field_name is valid and exists and that the delta is numeric and >=0.
 */
function ctools_cck_field_ctools_cck_field_relationship_settings_form_validate($form, $conf, $display) {
  // Validate the field name
  if (drupal_substr($conf['field_name'], 0, 6) == 'field_') {
    if (!($field_info = content_fields($conf['field_name']))) {
      form_set_error('field_name', t('A field by that name does not exist'));
    }
  }
  else {
    form_set_error('field_name', t('Field names must start with <code>field_</code>'));
  }

  // Validate the Delta
  if (preg_match('/[^0-9]/', $conf['delta'])) {
    form_set_error('delta', t('Delta must be numeric'));
  }
  elseif ($conf['delta'] < 0) {
    form_set_error('delta', t('Delta must be more than or equal to zero'));
  }

  // Validate the column
  if (isset($field_info) && !empty($field_info)) {
    if (!isset($field_info['columns'][$conf['column']])) {
      form_set_error('column', t('The column %column is not found on %field_name', array('%column' => $conf['column'], '%field_name' => $conf['field_name'])));
    }
  }
}
