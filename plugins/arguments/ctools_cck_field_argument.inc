<?php
/**
 * @file ctools_cck_field_argument.inc
 *
 * This file provides a CCK Argument context, useful for providing a node Context using a CCK Value from a placeholder in the URL.
 * For example:
 * book/%isbn
 *
 * The %isbn placeholder could be mapped to an ISBN CCK Field on the 'book' content type.
 */

$plugin = array(
  'title' => t("CCK Field"),
  'keyword' => 'cck_field',
  'description' => t('Adds a Node context based on a CCK Field argument'),
  'context' => 'ctools_cck_field_ctools_cck_field_argument_context',
  'default' => array('field_name' => '', 'delta' => 0),
  'settings form' => 'ctools_cck_field_ctools_cck_field_argument_context_settings_form',
  'settings form validate' => 'ctools_cck_field_ctools_cck_field_argument_context_settings_form_validate',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the CCK Field Name for this argument'),
  ),
  'js' => array('misc/autocomplete.js'),
);


/**
 * Generate the context for this argument
 */
function ctools_cck_field_ctools_cck_field_argument_context($arg = NULL, $conf = NULL, $empty = FALSE) {  
  // If unset it wants a generic, unfilled context.
  // Of if the field isn't set
  if ($empty || !isset($conf['field_name'])) {
    return ctools_context_create_empty('node');
  }

  // Get the field info
  $field = content_fields($conf['field_name']);
  $field_info = content_database_info($field);


  // Retrieve DB details
  $table = db_escape_string($field_info['table']);
  $column = db_escape_string($field_info['columns']['value']['column']);

  // NOTE: Coder flags this as an SQL Injection point, however the $table and $column values are escaped above.
  //       We cannot insert them using %s placeholders as db_query() runs db_prefix_tables() before replacing placeholders.
  //       This means table names would not be matched for prefixing.
  $query = "SELECT n.nid FROM {node} n INNER JOIN {{$table}} c ON n.vid = c.vid WHERE c.{{$column}} = '%s'";
  $args = array($arg);

  // Get the delta to find - if not set, assume zero
  $delta = isset($conf['delta']) ? $conf['delta'] : 0;
  // If delta is -1, we can pick from any matching delta in the values for a field.
  // If delta is more than -1 (ie, 0+) then we need to match against a specific delta.
  if ($conf['delta'] > -1) {
    // Check if the table for this field has a Delta, this ensures valid SQL
    $schema = drupal_get_schema($field_info['table']);
    if (isset($schema['fields']['delta'])) {
      $query .= " AND c.delta = %di ORDER BY c.delta ASC";
      $args[] = $delta;
    }
  }

  // Extract the Node ID. Only one will be returned. If there are multiple matches (multiple deltas), the first is returned.
  $nid = db_result(db_query_range(db_rewrite_sql($query), $args, 0, 1));

  // If no Node ID is returned, return an empty context
  if (!$nid) {
    return ctools_context_create_empty('node');
  }
  // Return a node context, if a $nid is available
  return ctools_context_create('node', node_load($nid));
}


/**
 * Settings form
 */
function ctools_cck_field_ctools_cck_field_argument_context_settings_form(&$form, &$form_state, $conf) {
  $form['settings']['field_name'] = array(
    '#title' => t('Field Name'),
    '#description' => t('Enter the machine name of the field to relate to. These always start <code>field_</code>. The field will autocomplete suggestions which contain the value you enter.'),
    '#type' => 'textfield',
    '#maxlength' => 32,
    '#size' => 32,
    '#autocomplete_path' => 'ctools/autocomplete/ctools-cck-field-fied-names',
    '#default_value' => $conf['field_name'],
    '#required' => TRUE,
  );

  $form['settings']['delta'] = array(
    '#title' => t('Delta'),
    '#description' => t('The <em>delta</em> represents the item of the field. This is a zero-indexed list (0 (zero) is first followed by 1, 2, 3, etc). If the field is not set to multiple, you should leave this as zero. You may also use <em>-1</em> to represent "any".'),
    '#type' => 'textfield',
    '#maxlength' => 2,
    '#size' => 2,
    '#default_value' => isset($conf['delta']) ? $conf['delta'] : 0,
    '#required' => TRUE,
  );
}


/**
 * Validate handler for the settings form. Ensure the field_name is valid and exists and that the delta is numeric and >=0.
 */
function ctools_cck_field_ctools_cck_field_argument_context_settings_form_validate($form, &$form_state) {
  $conf = &$form_state['values']['settings'];
  if (drupal_substr($conf['field_name'], 0, 6) == 'field_') {
    if (!content_fields($conf['field_name'])) {
      form_set_error('field_name', t('A field by that name does not exist'));
    }
  }
  else {
    form_set_error('field_name', t('Field names must start with <code>field_</code>'));
  }

  if (!preg_match('/^-?[0-9]+/', $conf['delta'])) {
    form_set_error('delta', t('Delta must be numeric'));
  }
  elseif ($conf['delta'] < -1) {
    form_set_error('delta', t('Delta must be more than or equal to zero, or -1 for "any"'));
  }
}

